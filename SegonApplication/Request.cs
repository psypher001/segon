﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace SegonApplication
{
    public class Request
    {
        public Request(HttpListenerRequest request)
        {
            StreamReader streamReader = new StreamReader(request.InputStream);
            string contentResult = streamReader.ReadToEndAsync().Result;
            Console.WriteLine(contentResult);
            Console.WriteLine(request.InputStream.ReadByte().ToString());
            Console.WriteLine(request.RemoteEndPoint.ToString());
            Console.WriteLine(request.TransportContext.ToString());
            Console.WriteLine(request.RequestTraceIdentifier.ToString());
            Console.WriteLine(request.UserAgent.ToString());
            streamReader.BaseStream.Flush();
            request.InputStream.Flush();
        }
    }
}
