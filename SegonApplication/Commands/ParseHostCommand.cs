﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace SegonApplication.Commands
{
    public sealed class ParseHostCommand : CommandDelegate
    {
        public String Url;
        public HtmlDocument htmlWebObject;
        public ParsedResults Results;

        public sealed class ParsedResults
        {
            public HtmlDocument htmlWebObject;
            public List<string> inboundLinks = new List<string>();
            public List<string> outboundLinks = new List<string>();
        }

        public ParseHostCommand( Action<ParsedResults> commandMethod) : base(commandMethod)
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            Results = new ParsedResults();
            htmlWebObject = new HtmlWeb().LoadFromWebAsync(Url).Result;

            commandMethod(Results);
        }
        
        public ParseHostCommand(string Url, Action<ParsedResults> commandMethod) : base(commandMethod)
        {
          
            HtmlDocument htmlDocument = new HtmlDocument();
            Results = new ParsedResults();
            htmlWebObject = new HtmlWeb().LoadFromWebAsync(Url).Result;
           
            Results.htmlWebObject = htmlWebObject;
            ParseLinks(Url);
            commandMethod(Results);
        }

        private async Task ParseLinks(string Url)
        {
            IEnumerable<HtmlNode> linkNodes = htmlWebObject.DocumentNode.SelectNodes("//a");
            foreach(HtmlNode node in linkNodes)
            {
                foreach (HtmlNode desc in node.ChildNodes)
                {
                    if(desc != null)
                    {
                        if (node.Attributes["href"].Value.Contains(Url) == true)
                        {
                            Results.inboundLinks.Add(node.Attributes["href"].Value);
                        }
                        else Results.outboundLinks.Add(node.Attributes["href"].Value);
                        Console.WriteLine(node.Attributes["href"].Value);
                    }
                }
            }
         
        }
    }
}
