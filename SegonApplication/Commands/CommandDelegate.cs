﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace SegonApplication
{
    public class CommandDelegate : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public Action<object> _CommandMethodParams;
        public Action  _CommandMethod;
        private object target;

        public CommandDelegate(Action<object> commandMethod)
        {
            _CommandMethodParams = commandMethod;
        }
        public CommandDelegate(Action commandMethod)
        {
            _CommandMethod = commandMethod;
        }

        public CommandDelegate(object target)
        {
            this.target = target;
        }

        public bool CanExecute(object parameter)  { return true; }
        public void Execute(object parameter) => _CommandMethodParams.Invoke(parameter);
        public void Execute() => _CommandMethod.Invoke();

    }
}
