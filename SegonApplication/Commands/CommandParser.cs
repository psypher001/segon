﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SegonApplication.Commands
{
    public class CommandParser
    {
        private static string cmd = "segon";

        public Arguments CommandArgs;
        public string LocalPort { get; set; }
        public string LocalEndpointUrl { get; set; }
        public string CommandName { get; set; }
        public string Defaults { get; set; }

        public CommandParser(string[] parameters)
        {
            foreach (string param in parameters)
            {
                if (param.ToLower().Trim() == cmd)
                {
                    LocalEndpointUrl = parameters[1].ToString();
                    Console.WriteLine(param);
                    break;
                }
                else Console.WriteLine(param);
            }
            //}+
            //CommandArgs = Arguments.ArgCommand;
            //CommandName = parameters.GetEnumerator().MoveNext().ToString();
            //LocalEndpointUrl = parameters.GetEnumerator().MoveNext().ToString();
            //LocalPort = parameters.GetEnumerator().MoveNext().ToString();

        }
    }

    public enum Arguments { ArgCommand = 0, ArgLocalEndpoint = 1, ArgPort = 2 }
}
