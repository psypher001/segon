﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace SegonApplication
{
    public class Response 
    {
        private HttpListenerResponse response;

        public Response(HttpListenerResponse responseHttp)
        {
            response = responseHttp;
        }

        public string Render(string filename)
        {
            StreamWriter streamWriter = new StreamWriter(response.OutputStream);

            string filecontents = File.ReadAllText(filename);
            response.ContentEncoding = streamWriter.Encoding;
            response.ContentLength64 = Encoding.Default.GetByteCount(filecontents);
            response.ContentType = "text/html";
            response.SendChunked = true;
            streamWriter.AutoFlush = true;
            streamWriter.Write(filecontents);
            streamWriter.Flush();
            response.OutputStream.Flush();
            response.Close();

            return filecontents;
        }

        public string Send(string message)
        {
            StreamWriter streamWriter = new StreamWriter(response.OutputStream);

            string responseString = message;
            response.ContentEncoding = streamWriter.Encoding;
            response.ContentLength64 = Encoding.Default.GetByteCount(responseString);
            response.ContentType = "text/html";
            response.SendChunked = true;
            streamWriter.AutoFlush = true;
            streamWriter.Write(responseString);
            streamWriter.Flush();
            response.OutputStream.Flush();
            response.Close();

            return responseString;
        }
    }
}
