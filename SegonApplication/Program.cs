﻿using SegonApplication.Commands;
using System;

namespace SegonApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandParser commandParser = new CommandParser(args);
           
            Client AppClient = new Client(commandParser.LocalEndpointUrl);
          
            AppClient.GetHttp("", (request, response) => {
                response.Render("index.html");
            });
            AppClient.GetHttp("home/", (request, response) => {
                response.Render("index.html");
            });


            Console.ReadLine();
        }
    }
}
