﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace SegonApplication
{
    public class Client
    {
        HttpListener httpListener = new HttpListener();
        HttpListenerContext httpContext;
        String LocalEndpoint;
        Boolean SetPrefixes = true;

        public Client(string localendpoint)
        {
            localendpoint = localendpoint == null ? "http://127.0.0.1:80/" : localendpoint;
            LocalEndpoint = localendpoint;
            Console.WriteLine(LocalEndpoint);
            httpListener.Prefixes.Add(LocalEndpoint);
            Console.WriteLine("Segon Server Online");
          
        }

        public void GetHttp(string path, Action<Request, Response> callback)
        {
            httpListener.Start();
            httpListener.Prefixes.Clear();
            if (SetPrefixes) httpListener.Prefixes.Add(LocalEndpoint + path);

            SetPrefixes = !SetPrefixes;
            httpListener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
            httpContext = httpListener.GetContextAsync().Result;
            HttpListenerRequest request = httpContext.Request;
            HttpListenerResponse response = httpContext.Response;
            
            if(request.HttpMethod == "GET" || request.HttpMethod == "POST")
            {
                Request requestHttp = new Request(request);
                Response responseHttp = new Response(response);
                callback(requestHttp, responseHttp);
            }
        
             GetHttp(path, callback);
        }
    }
}
