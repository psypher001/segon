CD C:\Users\User\source\repos\SegonApplication
ECHO "Building Target Platforms, Linux/Windows \n"
dotnet build SegonApplication -c Release -r win10-x64
ECHO "Publishing Windows \n"
dotnet publish SegonApplication -c Release -r win10-x64
ECHO "Finished publishing Windows! \n"
dotnet build SegonApplication -c Release -r ubuntu.14.04-x64
ECHO "Publishing Linux Ubuntu \n"
dotnet publish SegonApplication -c Release -r ubuntu.14.04-x64
ECHO "DONE \n"
ECHO "Now Updating GIT REPOS"
git add .
git commit -m "Minor updates"
git push -u origin master
ECHO "Successfully Finished Deployment Process!"
PAUSE